import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import itertools
import sys
import numpy as np
from urllib.parse import urlparse
import re

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()


def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)


import os

directory = os.fsencode(r'C:\Users\stefa\OneDrive\Bureaublad\dev\ajigo\problems\season_24\24_02')

def write_problem(filename, problem_number):
    meta = 'GM[1]FF[4]SZ[19]HA[0]KM[0]GN[AjiGO]'
    problem  = open(filename, "r").read()

    try:
        objective = re.search('C\[(.|\n)*?]',problem).group(0)
    except:
        objective = 'Black to play'
    white_stones = re.search(r'AW(\[[a-z][a-z]\])*', problem).group(0)
    black_stones = re.search(r'AB(\[[a-z][a-z]\])*', problem).group(0)

    all_stones = black_stones + white_stones


    problem_state_max_with = all_stones.replace('AB', '')
    problem_state_max_with = problem_state_max_with.replace('AW', '')
    problem_state_max_with = problem_state_max_with.split(']')
    #problem_state_max_with= [w.replace(']', '') for w in problem_state_max_with ]
    problem_state_max_with= [w.replace('[', '') for w in problem_state_max_with ]
    problem_state_max_with = list(filter(None, problem_state_max_with))

    temp_problem = problem.split('GN')[1]
    clean_problem = temp_problem.split(';')[0]
    temp_problem = temp_problem.replace(clean_problem, '')
    temp_problem = temp_problem.replace(')', '')

    temp_problem = temp_problem.replace('[Correct]', '[Correct]TE[1]')

    if temp_problem[1] == 'W':
        problem_state_max = all_stones.replace('AW', 'AX')
        problem_state_max = problem_state_max.replace('AB', 'AW')
        problem_state_max = problem_state_max.replace('AX', 'AB')
        solutions = temp_problem.replace(';B', ';X')
        solutions = solutions.replace(';W', ';B')
        solutions = solutions.replace(';X', ';W')
        solutions = solutions.replace('White', 'Black')
        solutions = objective + '(' + solutions + ')'
    else:
        problem_state_max = all_stones
        solutions = objective + '(' + temp_problem + ')'

    vw = 'VW[' + 'aa' + ':' + 'ss' + ']'
    meta = meta  + vw
    total_sgf = '(;' +  meta + problem_state_max  + solutions + ')'


    cur.execute('INSERT INTO public. "tsumego_tsumego_overview" ("unique_problem_identifier", "problem_number","problem","solution_1", "meta", "total_problem", "tsumego_rating", "tsumego_phi", "book", "problem_type") VALUES (%s,%s,%s,%s,%s,%s,%s, %s,%s, %s)', (problem_number+2402000, problem_number+2402000, problem_state_max, solutions, meta, total_sgf, 0, 0, 'In-Seong Lecture 3x3 invasion vol.2(2402)', 'Patterns'))

    conn.commit()

for idx, file in enumerate(os.listdir(directory)):
     filename = os.fsdecode(file)
     if filename.endswith(".sgf"):

         write_problem(filename, idx)
         # print(os.path.join(directory, filename))
         continue
     else:
         continue
