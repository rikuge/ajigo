import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import itertools
import sys
import numpy as np
from urllib.parse import urlparse

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()


def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)


import os

#directory = os.fsencode(r'C:\Users\stefa\Desktop\ajigo\ajigo\problems\cho_chikun_elementary')

directory = os.fsencode(r'C:\Users\stefa\OneDrive\Bureaublad\dev\ajigo\problems\cho_chikun_elementary')



def write_problem(filename, problem_number):
    problem  = open(filename, "r").read()

    meta = problem.split('(abc)]')[0].replace('(;', '') + '(abc)]'
    problem_state = problem.split('(abc)]')[1].split('C[')[0]
    solutions = problem.split('(abc)]')[1].replace(problem_state, '')
    solutions = solutions.replace('[Correct.])', '[Correct.]TE[1])').replace('))', ')')
    tsumego_rating = 1500
    tsumego_phi = 350

    problem_max = problem_state.split('[')[1:]
    problem_max = [w.replace(']AW', '') for w in problem_max ]
    problem_max = [w.replace(']', '') for w in problem_max ]
    #print(problem_max)
    first_letter = [word[0] for word in problem_max]
    second_letter = [word[1] for word in problem_max]
    position_1 = char_position(max(first_letter)) + 2
    position_2 = char_position(max(second_letter)) + 2

    char_1 = pos_to_char(position_1)
    char_2 = pos_to_char(position_2)

    vw = 'VW[aa:' + char_1 + char_2 + ']'
    meta = meta  + vw

    cur.execute('INSERT INTO public. "tsumego_tsumego_overview" ("unique_problem_identifier", "problem_number","problem","solution_1", "meta", "total_problem", "tsumego_rating", "tsumego_phi", "book", "problem_type") VALUES (%s,%s,%s,%s,%s,%s,%s, %s,%s, %s)', (problem_number+200001, problem_number+200001, problem_state, solutions, meta, '', 1, 0, 'Cho Chikun Elementary', 'L&D'))
    conn.commit()

for idx, file in enumerate(os.listdir(directory)):
     filename = os.fsdecode(file)
     if filename.endswith(".sgf"):
         write_problem(filename, idx)
         # print(os.path.join(directory, filename))
         continue
     else:
         continue
