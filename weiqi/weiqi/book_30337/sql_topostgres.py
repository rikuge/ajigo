import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import itertools
import sys
import numpy as np
import re
from urllib.parse import urlparse

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()

import pandas as pd
import sqlite3

def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)


import os


rating_mapper = {
                '18K': 300,  '18K+': 350, '17K': 400,  '17K+': 450, '16K': 500,  '16K+': 550,
                '15K': 600,  '15K+': 650, '14K': 700,  '14K+': 750, '13K': 800,  '13K+': 850,
                '12K': 900,  '12K+': 950, '11K': 1000,  '11K+': 1050, '10K': 1100,  '10K+': 1150,
                '9K': 1200,  '9K+': 1250, '8K': 1300,  '8K+': 1350, '7K': 1400,  '7K+': 1450,
                '6K': 1500,  '6K+': 1550, '5K': 1600,  '5K+': 1650, '4K': 1700,  '4K+': 1750,
                '3K': 1800,  '3K+': 1850, '2K': 1900,  '2K+': 1950, '1K': 2000,  '1K+': 2050,
                '1D': 2100,  '1D+': 2150, '2D': 2200,  '2D+': 2250, '3D': 2300,  '3D+': 2350,
                '4D': 2400,  '4D+': 2450, '5D': 2500,  '5D+': 2550, '6D': 2600,  '6D+': 2650,
                '7D': 2700,  '7D+': 2750, '8D': 2800,  '8D+': 2850, '9D': 2900,  '9D+': 2950,
                }


# Read sqlite query results into a pandas DataFrame
con = sqlite3.connect("./books.db")
df = pd.read_sql_query("SELECT * from book_1", con)
df['sgf'] = df['sgf'].str.replace('【正解图】', 'Correct')
df['sgf'] = df['sgf'].str.replace('黑先', 'Black to play')
df['sgf'] = df['sgf'].str.replace('白先', 'White to play')
df['sgf'] = df['sgf'].str.replace('正解', '[Correct]')
df['sgf'] = df['sgf'].str.replace('恭喜答对', '')
df['sgf'] = df['sgf'].str.replace('变化', '')
df['sgf'] = df['sgf'].str.replace('活（中级）', '')
df['sgf'] = df['sgf'].str.replace('White to play,White to play', 'White to play')
df['sgf'] = df['sgf'].str.replace('Black to play,Black to play', 'Black to play')

df = df.replace(r'\\n',' ', regex=True)
df['sgf'] = df['sgf'].str.replace(r'(C\[)(\w|\s)*(Correct).*?\]', 'C[Correct]', regex=True)
df['sgf'] = df['sgf'].str.replace('[Correct]', '[Correct]TE[1]', regex=False)
df['sgf'] = df['sgf'].str.replace('[ Correct]', '[Correct]TE[1]', regex=False)
df['sgf'] = df['sgf'].str.replace('[ Correct ]', '[Correct]TE[1]', regex=False)
df['sgf'] = df['sgf'].str.replace('[Correct ]', '[Correct]TE[1]', regex=False)
df['sgf'] = df['sgf'].str.replace('[  Correct]', '[Correct]TE[1]', regex=False)
df['sgf'] = df['sgf'].str.replace(r'[^\x00-\x7F]+', '')
df['sgf'] = df['sgf'].str.replace('White to play,White to play', 'White to play')
df['sgf'] = df['sgf'].str.replace('Black to play,Black to play', 'Black to play')
df['sgf'] = df['sgf'].str.replace('C[ ]', 'C[Continue]', regex=False)

df['qtypename'] = df['qtypename'].str.replace('死活题', 'L&D', regex=False)
df['qtypename'] = df['qtypename'].str.replace('手筋题', 'Tesuji', regex=False)
df['qtypename'] = df['qtypename'].str.replace('吃子题', 'Tesuji', regex=False)
df['qtypename'] = df['qtypename'].str.replace('官子题', 'Tesuji', regex=False)
df['qtypename'] = df['qtypename'].str.replace('欣赏题', 'Tesuji', regex=False)

print(df['qtypename'].value_counts())
df.replace({'levelname': rating_mapper}, inplace=True)

def write_problem(row, problem_number):
    meta = 'GM[1]FF[4]SZ[19]HA[0]KM[0]GN[AjiGO]'
    problem_state = row['sgf']

    problem_state_max = re.split('C\[(.|\n)*?]',problem_state)[0]
    solutions = problem_state.replace(problem_state_max, '')
    problem_state_max = problem_state_max.replace('(;', '')
    solutions = solutions.replace('))', ')')
    solutions = solutions.replace('C[ ]', 'C[Continue]')
    solutions = solutions.replace('C[]', 'C[Continue]')
    if "White" in solutions:
        problem_state_max = problem_state_max.replace('AW', 'AX')
        problem_state_max = problem_state_max.replace('AB', 'AW')
        problem_state_max = problem_state_max.replace('AX', 'AB')
        solutions = solutions.replace(';B', ';X')
        solutions = solutions.replace(';W', ';B')
        solutions = solutions.replace(';X', ';W')
        solutions = solutions.replace('White', 'Black')

    problem_state_max_with = problem_state_max.replace('AB', '')
    problem_state_max_with = problem_state_max_with.replace('AW', '')
    problem_state_max_with = problem_state_max_with.split(']')
    #problem_state_max_with= [w.replace(']', '') for w in problem_state_max_with ]
    problem_state_max_with= [w.replace('[', '') for w in problem_state_max_with ]
    #print(problem_state_max_with)
    problem_state_max_with = list(filter(None, problem_state_max_with))
    first_letter = [word[0] for word in problem_state_max_with]
    second_letter = [word[1] for word in problem_state_max_with]

    position_11 = char_position(min(first_letter)) - 2
    position_12 = char_position(min(second_letter)) - 2

    if position_11 < 2:
        position_11 = 0

    if position_12 < 2:
        position_12 = 0

    position_1 = char_position(max(first_letter)) + 2
    position_2 = char_position(max(second_letter)) + 2

    char_1 = pos_to_char(position_1)
    char_2 = pos_to_char(position_2)

    char_11 = pos_to_char(position_11)
    char_12 = pos_to_char(position_12)

    print(char_1)
    print(char_2)
    print(char_11)
    print(char_12)

    vw = 'VW[' + char_12 + char_11 + ':' + char_1 + char_2 + ']'
    meta = meta  + vw
    total_sgf = '(;' +  meta + problem_state_max + solutions + ')'

    print(total_sgf)
    cur.execute('INSERT INTO public. "tsumego_tsumego_overview" ("unique_problem_identifier", "problem_number","problem","solution_1", "meta", "total_problem", "tsumego_rating", "tsumego_phi", "book", "problem_type") VALUES (%s,%s,%s,%s,%s,%s,%s, %s,%s, %s)', (problem_number+3497, problem_number+3497, problem_state_max, solutions, meta, total_sgf, row['levelname'], 50, 'Yingang Go Academy', row['qtypename']))
    conn.commit()

for i, row in df.iterrows():
    write_problem(row, i)

con.close()
