from django.apps import AppConfig


class TsumegoSolverConfig(AppConfig):
    name = 'tsumego'
