# Generated by Django 2.1 on 2020-05-19 20:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tsumego', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tsumego_overview',
            name='meta',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
