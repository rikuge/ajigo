from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.utils import timezone

class tsumego_overview(models.Model):
    unique_problem_identifier = models.IntegerField(null=True, blank=True)
    problem_number = models.IntegerField(null=True, blank=True)
    total_problem = models.CharField(null=True, blank=True, max_length=50000)
    meta = models.CharField(null=True, blank=True, max_length=255)
    problem = models.CharField(null=True, blank=True, max_length=50000)
    solution_1 = models.CharField(null=True, blank=True, max_length=50000)
    tsumego_rating = models.IntegerField(null=True, blank=True)
    tsumego_phi = models.IntegerField(null=True, blank=True)
    book = models.CharField(null=True, blank=True, max_length=50000)
    problem_type = models.CharField(null=True, blank=True, max_length=50000)

class tsumego_stats(models.Model):
    unique_problem_identifier = models.IntegerField(null=True, blank=True)
    rank_rating = models.IntegerField(null=True, blank=True)
    book_number = models.IntegerField(null=True, blank=True)
    problem_stats = JSONField()
