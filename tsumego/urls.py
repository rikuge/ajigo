from django.urls import path, include
from django.contrib import admin

from .views import *
from django.views.generic import TemplateView

urlpatterns = [
    path('problems' , ProblemAPI.as_view(), name='Problems'),

]
