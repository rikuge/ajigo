from django.shortcuts import render
from django.views.generic import View
from django.db.models.functions import TruncHour, TruncDay, TruncMonth
import datetime
from datetime import timedelta

from django.db.models import Count, Sum, F
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework import viewsets, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import SessionAuthentication
from rest_framework import generics
from django_pandas.io import read_frame
from datetime import date, timedelta
import calendar
import pandas as pd
import datetime as dt

from django.views.decorators.csrf import csrf_exempt

from .models import *

class ProblemAPI(APIView):
    def get(self, request, format=None, *args, **kwargs):
        current_user = request.user.pk

        get_data = request.query_params
        qs = tsumego_overview.objects.all()
        if 'id' in get_data:
            qs = qs.filter(problem_number__iexact=(get_data['id']))
        df_total = read_frame(qs)
        problem = df_total['problem'].tolist()[0]
        solution = df_total['solution_1'].tolist()[0]
        problem_rating = df_total['tsumego_rating'].tolist()[0]
        problem_rating_phi = df_total['tsumego_phi'].tolist()[0]
        meta = df_total['meta'].tolist()[0]
        problem_total = "(;" + meta + problem + solution + ")"
        
        return Response({"problem": problem_total, "problem_rating": problem_rating, "problem_rating_phi": problem_rating_phi})
