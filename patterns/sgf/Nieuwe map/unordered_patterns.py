import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import random
import itertools
import sys
import numpy as np
from urllib.parse import urlparse
from collections import Counter

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
import re
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()


reversed_mapping = {
    'a' : 's',
    'b' : 'r',
    'c' : 'q',
    'd' : 'p',
    'e' : 'o',
    'f' : 'n',
    'g' : 'm',
    'h' : 'l',
    'i' : 'k',
    'j' : 'j',
    'k' : 'i',
    'l' : 'h',
    'm' : 'g',
    'n' : 'f',
    'o' : 'e',
    'p' : 'd',
    'q' : 'c',
    'r' : 'b',
    's' : 'a',
}




def remap_board_position(sequence):
    sequence_original = sequence
    move_list = re.findall('\[.*?\]',sequence)
    sequence_1 = ''
    sequence_2 = ''
    sequence_3 = ''
    for x in move_list:
        #use dict to look up value
        remap_point_1 = reversed_mapping.get(x[1])
        remap_point_2 = reversed_mapping.get(x[2])
        remapped_sequences_1 = '[' + remap_point_1 + remap_point_2 + ']'
        sequence_1 = sequence_1 + remapped_sequences_1
        remapped_sequences_2 = '[' + x[1] + remap_point_2 + ']'
        sequence_2 = sequence_2 + remapped_sequences_2
        remapped_sequences_3 = '[' + remap_point_1 + x[2] + ']'
        sequence_3 = sequence_3 +remapped_sequences_3
    alle_sequences = [sequence_1, sequence_2, sequence_3]
    return alle_sequences


def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)
from itertools import combinations

import os

directory = os.fsencode(r'C:\Users\Stefan\Desktop\ajigo\patterns\sgf\Nieuwe map')
alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's']
keywords = [''.join(i) for i in itertools.product(alphabets, repeat = 2)]
keywords = ["[" + keyword + "]" for keyword in keywords]


#50 - 25 - 25 - 25 - 20 - 20 - 20 - 15 - 15 - 15 - 10  > 0.1




def analyze_sgf(filename):
    game  = open(filename, "r",  encoding = "ISO-8859-1").read()
    try:
        actual_game = game.split('AP[foxwq]')[1]
        game_chunked = re.findall('\[.*?\]',actual_game)
        year = int(str(re.findall('DT\[(.*?)\]', game))[2:6])
        return [game_chunked, year]
    except:
        pass


def calculate_everything(focus_year):
    all_games_2019 = []
    all_games_2018 = []
    all_games_2017 = []
    all_games_2016 = []
    all_games_2015 = []
    all_games_2014 = []
    all_games_2013 = []
    all_games_2012 = []

    for idx, file in enumerate(os.listdir(directory)):

         filename = os.fsdecode(file)
         if filename.endswith(".sgf"):
             game = analyze_sgf(filename)
             if game is not None:
                 year = game[1]
                 if year == 2019:
                     all_games_2019.extend([game[0]][:50])
                 elif year == 2018:
                     all_games_2018.extend([game[0]][:50])
                 elif year == 2017:
                     all_games_2017.extend([game[0]][:50])
                 elif year == 2016:
                     all_games_2016.extend([game[0]][:50])
                 elif year == 2015:
                     all_games_2015.extend([game[0]][:50])
                 elif year == 2014:
                     all_games_2014.extend([game[0]][:50])
                 elif year == 2013:
                     all_games_2013.extend([game[0]][:50])
                 elif year == 2012:
                     all_games_2012.extend([game[0]][:50])
                 else:
                     pass
             else:
                 continue
             # print(os.path.join(directory, filename))
             continue
         else:
             continue

    random.shuffle(all_games_2019)
    random.shuffle(all_games_2018)
    random.shuffle(all_games_2017)
    random.shuffle(all_games_2016)
    random.shuffle(all_games_2015)
    random.shuffle(all_games_2014)
    random.shuffle(all_games_2013)
    random.shuffle(all_games_2012)

    all_games_2019 = all_games_2019[:2500]
    all_games_2018 = all_games_2018[:2500]
    all_games_2017 = all_games_2017[:2500]
    all_games_2016 = all_games_2016[:2500]
    all_games_2015 = all_games_2015[:2500]
    all_games_2014 = all_games_2014[:2500]
    all_games_2013 = all_games_2013[:2500]
    all_games_2012 = all_games_2012[:2500]

    if focus_year == 2019:
         all_games = all_games_2019
    elif focus_year == 2018:
        all_games = all_games_2018
    elif focus_year == 2017:
        all_games = all_games_2017
    elif focus_year == 2016:
        all_games = all_games_2016
    elif focus_year == 2015:
        all_games = all_games_2015
    elif focus_year == 2014:
        all_games = all_games_2014
    elif focus_year == 2013:
        all_games = all_games_2013
    elif focus_year == 2012:
        all_games = all_games_2012

    print(len(all_games))

    final = [x for x in all_games if x is not None]
    total_amount_of_games = len(final)
    final_flat = [item for sublist in final for item in sublist]

    d = Counter(final_flat)
    df = pd.DataFrame.from_dict(d, orient='index').reset_index()
    df = df.sort_values(0, ascending = False)
    most_played_moves = df['index'].tolist()[:50]

    #Calculate top two moves
    most_played_dict = []
    for x in most_played_moves:
        temp_filters = [x]
        final_temp = [i for i in final if any(b in temp_filters for b in i)]
        for y in keywords:
            if y != x:
                unsorted_list = [x, y]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict.extend([{'moves': [unsorted_list[0],unsorted_list[1]], 'count': amount_of_games_pattern}])
            else:
                pass

    df_two_moves = pd.DataFrame(most_played_dict)
    print('TWO MOVES')
    df_two_moves['moves_str'] = df_two_moves['moves'].astype(str)
    df_two_moves = df_two_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_two_moves = df_two_moves.sort_values('count', ascending=False)
    two_moves_list = df_two_moves['moves'].tolist()[:250]
    if focus_year == 2012:
        df_two_moves.to_csv('two_2012.csv', sep=';')
    elif focus_year == 2013:
        df_two_moves.to_csv('two_2013.csv', sep=';')
    elif focus_year == 2014:
        df_two_moves.to_csv('two_2014.csv', sep=';')
    elif focus_year == 2015:
        df_two_moves.to_csv('two_2015.csv', sep=';')
    elif focus_year == 2016:
        df_two_moves.to_csv('two_2016.csv', sep=';')
    elif focus_year == 2017:
        df_two_moves.to_csv('two_2017.csv', sep=';')
    elif focus_year == 2018:
        df_two_moves.to_csv('two_2018.csv', sep=';')
    elif focus_year == 2019:
        df_two_moves.to_csv('two_2019.csv', sep=';')
    else:
        pass
    #df_two_moves.to_csv('hoe.csv', sep=';')

    #Calculate top two moves
    most_played_dict_3 = []
    for z in two_moves_list:
        temp_filters_1 = [z[0]]
        temp_filters_2 = [z[1]]
        final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
        for y in keywords:
            if y not in z:
                unsorted_list = [y, z[0], z[1]]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict_3.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2]], 'count': amount_of_games_pattern}])
            else:
                pass

    print('THREE MOVES')
    df_three_moves = pd.DataFrame(most_played_dict_3 )
    df_three_moves['perc'] = (df_three_moves['count'] / total_amount_of_games) * 100
    df_three_moves['perc'] = df_three_moves['perc'].astype(int)

    df_three_moves['moves_str'] = df_three_moves['moves'].astype(str)

    df_three_moves = df_three_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_three_moves = df_three_moves.sort_values('count', ascending=False)
    df_three_moves = df_three_moves[df_three_moves['perc'] > 39]

    if focus_year == 2012:
        df_three_moves.to_csv('three_2012.csv', sep=';')
    elif focus_year == 2013:
        df_three_moves.to_csv('three_2013.csv', sep=';')
    elif focus_year == 2014:
        df_three_moves.to_csv('three_2014.csv', sep=';')
    elif focus_year == 2015:
        df_three_moves.to_csv('three_2015.csv', sep=';')
    elif focus_year == 2016:
        df_three_moves.to_csv('three_2016.csv', sep=';')
    elif focus_year == 2017:
        df_three_moves.to_csv('three_2017.csv', sep=';')
    elif focus_year == 2018:
        df_three_moves.to_csv('three_2018.csv', sep=';')
    elif focus_year == 2019:
        df_three_moves.to_csv('three_2019.csv', sep=';')
    else:
        pass
    three_moves_list = df_three_moves['moves'].tolist()[:1000]


    most_played_dict_4 = []
    for z in three_moves_list:
        temp_filters_1 = [z[0]]
        temp_filters_2 = [z[1]]
        temp_filters_3 = [z[2]]
        final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
        for y in keywords:
            if y not in z:
                unsorted_list = [y, z[0], z[1], z[2]]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict_4.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3]], 'count': amount_of_games_pattern}])
            else:
                pass

    print('FOUR MOVES')
    df_four_moves = pd.DataFrame(most_played_dict_4 )
    df_four_moves['perc'] = (df_four_moves['count'] / total_amount_of_games) * 100
    df_four_moves['perc'] = df_four_moves['perc'].astype(int)

    df_four_moves['moves_str'] = df_four_moves['moves'].astype(str)

    df_four_moves = df_four_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_four_moves = df_four_moves.sort_values('count', ascending=False)

    if focus_year == 2012:
        df_four_moves.to_csv('four_2012.csv', sep=';')
    elif focus_year == 2013:
        df_four_moves.to_csv('four_2013.csv', sep=';')
    elif focus_year == 2014:
        df_four_moves.to_csv('four_2014.csv', sep=';')
    elif focus_year == 2015:
        df_four_moves.to_csv('four_2015.csv', sep=';')
    elif focus_year == 2016:
        df_four_moves.to_csv('four_2016.csv', sep=';')
    elif focus_year == 2017:
        df_four_moves.to_csv('four_2017.csv', sep=';')
    elif focus_year == 2018:
        df_four_moves.to_csv('four_2018.csv', sep=';')
    elif focus_year == 2019:
        df_four_moves.to_csv('four_2019.csv', sep=';')
    else:
        pass
    four_moves_list = df_four_moves['moves'].tolist()[:1000]



    most_played_dict_5 = []
    for z in four_moves_list:
        temp_filters_1 = [z[0]]
        temp_filters_2 = [z[1]]
        temp_filters_3 = [z[2]]
        temp_filters_4 = [z[3]]
        final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_4 for b in i)]
        for y in keywords:
            if y not in z:
                unsorted_list = [y, z[0], z[1], z[2], z[3]]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict_5.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3], unsorted_list[4]], 'count': amount_of_games_pattern}])
            else:
                pass

    print('FIVES MOVES')
    df_five_moves = pd.DataFrame(most_played_dict_5)
    df_five_moves['perc'] = (df_five_moves['count'] / total_amount_of_games) * 100
    df_five_moves['perc'] = df_five_moves['perc'].astype(int)

    df_five_moves['moves_str'] = df_five_moves['moves'].astype(str)

    df_five_moves = df_five_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_five_moves = df_five_moves.sort_values('count', ascending=False)
    five_moves_list = df_five_moves['moves'].tolist()[:1000]
    if focus_year == 2012:
        df_five_moves.to_csv('five_2012.csv', sep=';')
    elif focus_year == 2013:
        df_five_moves.to_csv('five_2013.csv', sep=';')
    elif focus_year == 2014:
        df_five_moves.to_csv('five_2014.csv', sep=';')
    elif focus_year == 2015:
        df_five_moves.to_csv('five_2015.csv', sep=';')
    elif focus_year == 2016:
        df_five_moves.to_csv('five_2016.csv', sep=';')
    elif focus_year == 2017:
        df_five_moves.to_csv('five_2017.csv', sep=';')
    elif focus_year == 2018:
        df_five_moves.to_csv('five_2018.csv', sep=';')
    elif focus_year == 2019:
        df_five_moves.to_csv('five_2019.csv', sep=';')
    else:
        pass


    most_played_dict_6 = []
    for z in five_moves_list:
        temp_filters_1 = [z[0]]
        temp_filters_2 = [z[1]]
        temp_filters_3 = [z[2]]
        temp_filters_4 = [z[3]]
        temp_filters_5 = [z[4]]
        final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_4 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_5 for b in i)]
        for y in keywords:
            if y not in z:
                unsorted_list = [y, z[0], z[1], z[2], z[3], z[4]]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict_6.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3], unsorted_list[4], unsorted_list[5]], 'count': amount_of_games_pattern}])
            else:
                pass

    print('SIX MOVES')
    df_six_moves = pd.DataFrame(most_played_dict_6)
    df_six_moves['perc'] = (df_six_moves['count'] / total_amount_of_games) * 100
    df_six_moves['perc'] = df_six_moves['perc'].astype(int)

    df_six_moves['moves_str'] = df_six_moves['moves'].astype(str)

    df_six_moves = df_six_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_six_moves = df_six_moves.sort_values('count', ascending=False)
    six_moves_list = df_six_moves['moves'].tolist()[:100]

    if focus_year == 2012:
        df_six_moves.to_csv('six_2012.csv', sep=';')
    elif focus_year == 2013:
        df_six_moves.to_csv('six_2013.csv', sep=';')
    elif focus_year == 2014:
        df_six_moves.to_csv('six_2014.csv', sep=';')
    elif focus_year == 2015:
        df_six_moves.to_csv('six_2015.csv', sep=';')
    elif focus_year == 2016:
        df_six_moves.to_csv('six_2016.csv', sep=';')
    elif focus_year == 2017:
        df_six_moves.to_csv('six_2017.csv', sep=';')
    elif focus_year == 2018:
        df_six_moves.to_csv('six_2018.csv', sep=';')
    elif focus_year == 2019:
        df_six_moves.to_csv('six_2019.csv', sep=';')
    else:
        pass

    most_played_dict_7 = []
    for z in five_moves_list:
        temp_filters_1 = [z[0]]
        temp_filters_2 = [z[1]]
        temp_filters_3 = [z[2]]
        temp_filters_4 = [z[3]]
        temp_filters_5 = [z[4]]
        temp_filters_5 = [z[5]]
        final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_4 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_5 for b in i)]
        final_temp = [i for i in final_temp if any(b in temp_filters_6 for b in i)]
        for y in keywords:
            if y not in z:
                unsorted_list = [y, z[0], z[1], z[2], z[3], z[4], z[5]]
                temp_filters_2 = [y]
                final_temp_2 = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
                amount_of_games_pattern = len(final_temp_2)
                unsorted_list.sort()
                most_played_dict_7.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3], unsorted_list[4], unsorted_list[5], unsorted_list[6]], 'count': amount_of_games_pattern}])
            else:
                pass

    print('SEVEN MOVES')
    df_seven_moves = pd.DataFrame(most_played_dict_7)
    df_seven_moves['perc'] = (df_seven_moves['count'] / total_amount_of_games) * 100
    df_seven_moves['perc'] = df_seven_moves['perc'].astype(int)

    df_seven_moves['moves_str'] = df_seven_moves['moves'].astype(str)

    df_seven_moves = df_seven_moves.drop_duplicates(subset=['moves_str'], keep='first')
    df_seven_moves = df_seven_moves.sort_values('count', ascending=False)
    seven_moves_list = df_seven_moves['moves'].tolist()[:100]

    if focus_year == 2012:
        df_seven_moves.to_csv('seven_2012.csv', sep=';')
    elif focus_year == 2013:
        df_seven_moves.to_csv('seven_2013.csv', sep=';')
    elif focus_year == 2014:
        df_seven_moves.to_csv('seven_2014.csv', sep=';')
    elif focus_year == 2015:
        df_seven_moves.to_csv('seven_2015.csv', sep=';')
    elif focus_year == 2016:
        df_seven_moves.to_csv('seven_2016.csv', sep=';')
    elif focus_year == 2017:
        df_seven_moves.to_csv('seven_2017.csv', sep=';')
    elif focus_year == 2018:
        df_seven_moves.to_csv('seven_2018.csv', sep=';')
    elif focus_year == 2019:
        df_seven_moves.to_csv('seven_2019.csv', sep=';')
    else:
        pass



calculate_everything(2014)
