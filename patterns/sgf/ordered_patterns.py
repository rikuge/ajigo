import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import itertools
import sys
import numpy as np
from urllib.parse import urlparse
from collections import Counter

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
import re
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()


reversed_mapping = {
    'a' : 's',
    'b' : 'r',
    'c' : 'q',
    'd' : 'p',
    'e' : 'o',
    'f' : 'n',
    'g' : 'm',
    'h' : 'l',
    'i' : 'k',
    'j' : 'j',
    'k' : 'i',
    'l' : 'h',
    'm' : 'g',
    'n' : 'f',
    'o' : 'e',
    'p' : 'd',
    'q' : 'c',
    'r' : 'b',
    's' : 'a',
}




def remap_board_position(sequence):
    sequence_original = sequence
    move_list = re.findall('\[.*?\]',sequence)
    sequence_1 = ''
    sequence_2 = ''
    sequence_3 = ''
    for x in move_list:
        #use dict to look up value
        remap_point_1 = reversed_mapping.get(x[1])
        remap_point_2 = reversed_mapping.get(x[2])
        remapped_sequences_1 = '[' + remap_point_1 + remap_point_2 + ']'
        sequence_1 = sequence_1 + remapped_sequences_1
        remapped_sequences_2 = '[' + x[1] + remap_point_2 + ']'
        sequence_2 = sequence_2 + remapped_sequences_2
        remapped_sequences_3 = '[' + remap_point_1 + x[2] + ']'
        sequence_3 = sequence_3 +remapped_sequences_3
    alle_sequences = [sequence_1, sequence_2, sequence_3]
    return alle_sequences


def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)
from itertools import combinations

import os

directory = os.fsencode(r'C:\Users\Stefan\Desktop\ajigo\patterns\sgf')


twelve_move_comb_all = []

def analyze_sgf(filename):
    game  = open(filename, "r",  encoding = "ISO-8859-1").read()
    try:
        actual_game = game.split('AP[foxwq]')[1]
        game_chunked = re.findall('\[.*?\]',actual_game)
        for idx, move in enumerate(game_chunked):
            three_move_comb = game_chunked[idx:idx+3]
            four_move_comb = game_chunked[idx:idx+4]
            five_move_comb = game_chunked[idx:idx+5]
            six_move_comb = game_chunked[idx:idx+6]
            seven_move_comb = game_chunked[idx:idx+7]
            eight_move_comb = game_chunked[idx:idx+8]
            nine_move_comb = game_chunked[idx:idx+9]
            ten_move_comb = game_chunked[idx:idx+10]
            eleven_move_comb = game_chunked[idx:idx+11]
            twelve_move_comb = game_chunked[idx:idx+6]
            twelve_move_comb_all.extend([twelve_move_comb])

        return 0
    except:
        return 1



counter = 0
for idx, file in enumerate(os.listdir(directory)):
     print(counter)
     filename = os.fsdecode(file)
     if filename.endswith(".sgf"):
         counter_number = analyze_sgf(filename)
         counter+= counter_number
         # print(os.path.join(directory, filename))
         continue
     else:
         continue


twelve_move_comb_all= [''.join(x) for x in twelve_move_comb_all]
print(twelve_move_comb_all[0])

c = Counter(twelve_move_comb_all)

df = pd.DataFrame.from_dict(c, orient='index').reset_index()
print(df.shape)
df = df[df[0] > 25]
print(df.shape)
df = df.sort_values(0, ascending = False)
max_length = df['index'].map(lambda x: len(x)).max()
mask = (df['index'].str.len() == max_length)
df = df.loc[mask]
unique_combs = df['index'].tolist()
print(unique_combs[0])

twelve_move_comb_all = [x for x in twelve_move_comb_all if x in unique_combs]
print(len(twelve_move_comb_all))
#print(df['index'].value_counts().head(10))
#df[:500].to_csv('ok.csv')

additional_combinations = []
for g in twelve_move_comb_all:
    additional_combinations.extend(remap_board_position(g))

twelve_move_comb_all = twelve_move_comb_all + additional_combinations


move_list_exp_all = []
for move in twelve_move_comb_all:
    move_list = re.findall('\[.*?\]',move)
    move_list_exp = []
    for idx, x in enumerate(move_list):
        if idx % 2 == 0:
            move_list_exp.extend([';B' + x])
        else:
            move_list_exp.extend([';W' + x])
    move_list_exp = ''.join(move_list_exp)
    move_list_exp_all.extend([move_list_exp])

d = Counter(move_list_exp_all)

df = pd.DataFrame.from_dict(d, orient='index').reset_index()
print(df.shape)
df = df[df[0] > 2]
print(df.shape)
df = df.sort_values(0, ascending = False)
max_length = df['index'].map(lambda x: len(x)).max()
mask = (df['index'].str.len() == max_length)
df = df.loc[mask]
df.to_csv('wat.csv', sep=';')
