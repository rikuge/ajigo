import psycopg2
import calendar
import time
import json
import logging
import pytz
import calendar
import datetime
import pandas as pd
import datetime as dt
from time import mktime
from dateutil.relativedelta import relativedelta
import datetime
import codecs
import datetime
from datetime import date
from urllib.parse import urlparse
from psycopg2.extras import Json
import itertools
import sys
import numpy as np
from urllib.parse import urlparse
from collections import Counter

from sqlalchemy import create_engine
import os
from sqlalchemy import create_engine
import re
conn = psycopg2.connect('postgres://postgres:2564asAS!@localhost:5432/tsumego')

cur = conn.cursor()


reversed_mapping = {
    'a' : 's',
    'b' : 'r',
    'c' : 'q',
    'd' : 'p',
    'e' : 'o',
    'f' : 'n',
    'g' : 'm',
    'h' : 'l',
    'i' : 'k',
    'j' : 'j',
    'k' : 'i',
    'l' : 'h',
    'm' : 'g',
    'n' : 'f',
    'o' : 'e',
    'p' : 'd',
    'q' : 'c',
    'r' : 'b',
    's' : 'a',
}




def remap_board_position(sequence):
    sequence_original = sequence
    move_list = re.findall('\[.*?\]',sequence)
    sequence_1 = ''
    sequence_2 = ''
    sequence_3 = ''
    for x in move_list:
        #use dict to look up value
        remap_point_1 = reversed_mapping.get(x[1])
        remap_point_2 = reversed_mapping.get(x[2])
        remapped_sequences_1 = '[' + remap_point_1 + remap_point_2 + ']'
        sequence_1 = sequence_1 + remapped_sequences_1
        remapped_sequences_2 = '[' + x[1] + remap_point_2 + ']'
        sequence_2 = sequence_2 + remapped_sequences_2
        remapped_sequences_3 = '[' + remap_point_1 + x[2] + ']'
        sequence_3 = sequence_3 +remapped_sequences_3
    alle_sequences = [sequence_1, sequence_2, sequence_3]
    return alle_sequences


def char_position(letter):
    return ord(letter) - 97

def pos_to_char(pos):
    return chr(pos + 97)
from itertools import combinations

import os

directory = os.fsencode(r'C:\Users\Stefan\Desktop\ajigo\patterns\sgf')
alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's']
keywords = [''.join(i) for i in itertools.product(alphabets, repeat = 2)]
keywords = ["[" + keyword + "]" for keyword in keywords]


#50 - 25 - 25 - 25 - 20 - 20 - 20 - 15 - 15 - 15 - 10  > 0.1

def analyze_sgf(filename):
    game  = open(filename, "r",  encoding = "ISO-8859-1").read()
    try:
        actual_game = game.split('AP[foxwq]')[1]
        game_chunked = re.findall('\[.*?\]',actual_game)
    #print([s for s in game_chunked if pattern in s])


        return game_chunked
    except:
        pass


all_games = []
counter = 0
for idx, file in enumerate(os.listdir(directory)):

     filename = os.fsdecode(file)
     if filename.endswith(".sgf"):
         game = analyze_sgf(filename)
         if game is not None:
             print(idx)
             all_games.extend([game][:50])
         else:
             continue
         # print(os.path.join(directory, filename))
         continue
     else:
         continue





final = [x for x in all_games if x is not None]
total_amount_of_games = len(final)
final_flat = [item for sublist in final for item in sublist]

d = Counter(final_flat)
df = pd.DataFrame.from_dict(d, orient='index').reset_index()
df = df.sort_values(0, ascending = False)
most_played_moves = df['index'].tolist()[:50]

#Calculate top two moves
most_played_dict = []
for x in most_played_moves:
    temp_filters = [x]
    final_temp = [i for i in final if any(b in temp_filters for b in i)]
    #Hier een flatten list + counter
    for y in keywords:
        if y != x:
            unsorted_list = [x, y]
            temp_filters_2 = [y]
            final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
            amount_of_games_pattern = len(final_temp_2)
            unsorted_list.sort()
            most_played_dict.extend([{'moves': [unsorted_list[0],unsorted_list[1]], 'count': amount_of_games_pattern}])
        else:
            pass

df_two_moves = pd.DataFrame(most_played_dict)
print('TWO MOVES')
df_two_moves['moves_str'] = df_two_moves['moves'].astype(str)
df_two_moves = df_two_moves.drop_duplicates(subset=['moves_str'], keep='first')
df_two_moves = df_two_moves.sort_values('count', ascending=False)
two_moves_list = df_two_moves['moves'].tolist()[:250]
df_two_moves.to_csv('two.csv', sep=';')

#df_two_moves.to_csv('hoe.csv', sep=';')

#Calculate top two moves
most_played_dict_3 = []
for z in two_moves_list:
    temp_filters_1 = [z[0]]
    temp_filters_2 = [z[1]]
    final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
    #Hier een flatten list + counter
    for y in keywords:
        if y not in z:
            unsorted_list = [y, z[0], z[1]]
            temp_filters_2 = [y]
            final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
            amount_of_games_pattern = len(final_temp_2)
            unsorted_list.sort()
            most_played_dict_3.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2]], 'count': amount_of_games_pattern}])
        else:
            pass

print('THREE MOVES')
df_three_moves = pd.DataFrame(most_played_dict_3 )
df_three_moves['perc'] = (df_three_moves['count'] / total_amount_of_games) * 100
df_three_moves['perc'] = df_three_moves['perc'].astype(int)

df_three_moves['moves_str'] = df_three_moves['moves'].astype(str)

df_three_moves = df_three_moves.drop_duplicates(subset=['moves_str'], keep='first')
df_three_moves = df_three_moves.sort_values('count', ascending=False)
df_three_moves = df_three_moves[df_three_moves['perc'] > 39]
df_three_moves.to_csv('three.csv', sep=';')
three_moves_list = df_three_moves['moves'].tolist()[:1000]


most_played_dict_4 = []
for z in three_moves_list:
    temp_filters_1 = [z[0]]
    temp_filters_2 = [z[1]]
    temp_filters_3 = [z[2]]
    final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
    for y in keywords:
        if y not in z:
            unsorted_list = [y, z[0], z[1], z[2]]
            temp_filters_2 = [y]
            final_temp_2 = [i for i in final_temp  if any(b in temp_filters_2 for b in i)]
            amount_of_games_pattern = len(final_temp_2)
            unsorted_list.sort()
            most_played_dict_4.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3]], 'count': amount_of_games_pattern}])
        else:
            pass

print('FOUR MOVES')
df_four_moves = pd.DataFrame(most_played_dict_4 )
df_four_moves['perc'] = (df_four_moves['count'] / total_amount_of_games) * 100
df_four_moves['perc'] = df_four_moves['perc'].astype(int)

df_four_moves['moves_str'] = df_four_moves['moves'].astype(str)

df_four_moves = df_four_moves.drop_duplicates(subset=['moves_str'], keep='first')
df_four_moves = df_four_moves.sort_values('count', ascending=False)

df_four_moves.to_csv('four_moves.csv', sep=';')
#df_four_moves = df_four_moves[df_four_moves['perc'] > 39]

four_moves_list = df_four_moves['moves'].tolist()[:1000]



most_played_dict_5 = []
for z in four_moves_list:
    temp_filters_1 = [z[0]]
    temp_filters_2 = [z[1]]
    temp_filters_3 = [z[2]]
    temp_filters_4 = [z[3]]
    final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_4 for b in i)]
    for y in keywords:
        if y not in z:
            unsorted_list = [y, z[0], z[1], z[2], z[3]]
            temp_filters_2 = [y]
            final_temp_2 = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
            amount_of_games_pattern = len(final_temp_2)
            unsorted_list.sort()
            most_played_dict_5.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3], unsorted_list[4]], 'count': amount_of_games_pattern}])
        else:
            pass

print('FIVES MOVES')
df_five_moves = pd.DataFrame(most_played_dict_5)
df_five_moves['perc'] = (df_five_moves['count'] / total_amount_of_games) * 100
df_five_moves['perc'] = df_five_moves['perc'].astype(int)

df_five_moves['moves_str'] = df_five_moves['moves'].astype(str)

df_five_moves = df_five_moves.drop_duplicates(subset=['moves_str'], keep='first')
df_five_moves = df_five_moves.sort_values('count', ascending=False)
five_moves_list = df_five_moves['moves'].tolist()[:1000]

df_five_moves.to_csv('five_moves.csv', sep=';')



most_played_dict_6 = []
for z in five_moves_list:
    temp_filters_1 = [z[0]]
    temp_filters_2 = [z[1]]
    temp_filters_3 = [z[2]]
    temp_filters_4 = [z[3]]
    temp_filters_5 = [z[4]]
    final_temp = [i for i in final if any(b in temp_filters_1 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_3 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_4 for b in i)]
    final_temp = [i for i in final_temp if any(b in temp_filters_5 for b in i)]
    for y in keywords:
        if y not in z:
            unsorted_list = [y, z[0], z[1], z[2], z[3], z[4]]
            temp_filters_2 = [y]
            final_temp_2 = [i for i in final_temp if any(b in temp_filters_2 for b in i)]
            amount_of_games_pattern = len(final_temp_2)
            unsorted_list.sort()
            most_played_dict_6.extend([{'moves': [unsorted_list[0],unsorted_list[1], unsorted_list[2], unsorted_list[3], unsorted_list[4], unsorted_list[5]], 'count': amount_of_games_pattern}])
        else:
            pass

print('SIX MOVES')
df_six_moves = pd.DataFrame(most_played_dict_6)
df_six_moves['perc'] = (df_six_moves['count'] / total_amount_of_games) * 100
df_six_moves['perc'] = df_six_moves['perc'].astype(int)

df_six_moves['moves_str'] = df_six_moves['moves'].astype(str)

df_six_moves = df_six_moves.drop_duplicates(subset=['moves_str'], keep='first')
df_six_moves = df_six_moves.sort_values('count', ascending=False)
six_moves_list = df_six_moves['moves'].tolist()[:1000]

df_six_moves.to_csv('six_moves.csv', sep=';')

