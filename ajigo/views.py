from django.views.generic import View
from django.shortcuts import render, redirect


class IndexRenderView(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated == True:
            return render(request, 'index.html', {})
        else:
            return render(request, 'login.html', {})

class ProblemRenderView(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated == True:
            return render(request, 'problems.html', {})
        else:
            return render(request, 'login.html', {})
