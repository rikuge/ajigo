from django.urls import path, include
from django.contrib import admin

from .views import *
from django.views.generic import TemplateView

urlpatterns = [
    path('register/tsumego_data/' , TsumegoBasicData.as_view(), name='TsumegoData'),
    path('next/' , DetermineNextProblem.as_view(), name='DetermineNextProblem'),
    path('register/tsumego_extended_data/' , TsumegoExtendedData.as_view(), name='TsumegoExtendedData'),
    path('tsumego_stats/' , TsumegoUserStats.as_view(), name='TsumegoUserStats'),
    path('community_stats/', TsumegoCommunityStats.as_view(), name='TsumegoCommunityStats'),
    path('overall_stats/', TsumegoOverallStats.as_view(), name='TsumegoOverallStats'),


]
