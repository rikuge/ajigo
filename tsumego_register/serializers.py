from rest_framework import serializers
from rest_framework.serializers import (
    HyperlinkedIdentityField,
    HyperlinkedRelatedField,
    ModelSerializer,
)

from .models import *
from django.conf import settings


class TsumegoDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = tsumego_user_data
        fields = ("unique_problem_identifier", "user_id", "first_seen", "last_seen", "scheduled_spaced", "last_interval")


class TsumegoExtendedDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = tsumego_user_detail_data
        fields = ("unique_problem_identifier", "user_id", "time_spent", "effort", "correct", "answer_date", "attempts", "hints_used")
