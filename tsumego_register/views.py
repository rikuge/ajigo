from django.shortcuts import render
from django.views.generic import View
from django.db.models.functions import TruncHour, TruncDay, TruncMonth
import datetime
from datetime import timedelta

from django.db.models import Count, Sum, F
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework import viewsets, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import SessionAuthentication
from rest_framework import generics
from django_pandas.io import read_frame
from datetime import date, timedelta
from django.db.models import Q
import random
from random import randrange, uniform


import calendar
import pandas as pd
import datetime as dt

from .models import *
from .serializers import *
from django.views.decorators.csrf import csrf_exempt
from tsumego.models import tsumego_overview
from user_stats.models import user_progress

rating_mapper = {
                 300: '18K',  350: '18K+', 400: '17K',  450: '17K+', 500: '16K', 550: '16K+',
                 600: '15K',  650: '15K+', 700: '14K',  750: '14K+', 800: '13K', 850: '13K+',
                 900: '12K',  950: '12K+', 1000: '11K', 1050: '11K+', 1100: '10K', 1150: '10K+',
                 1200: '9K',  1250: '9K+', 1300: '8K',  1350: '8K+', 1400: '7K', 1450: '7K+',
                 1500: '6K',  1550: '6K+', 1600: '5K',  1650: '5K+', 1700: '4K', 1750: '4K+',
                 1800: '3K',  1850: '3K+', 1900: '2K',  1950: '2K+', 2000: '1K', 2050: '1K+',
                 2100: '1D',  2150: '1D+', 2200: '2D',  2250: '2D+', 2300: '3D', 2350: '3D+',
                 2400: '4D',  2450: '4D+', 2500: '5D',  2550: '5D+', 2600: '6D', 2650: '6D+',
                 2700: '7D',  2750: '7D+', 2800: '8D',  2850: '8D+', 2900: '9D', 2950: '9D+',
                }


class TsumegoBasicData(APIView):
    def get_object(self, problem_id, user_id):
        try:
            return tsumego_user_data.objects.get(unique_problem_identifier=problem_id, user_id=user_id)
        except:
            pass

    def put(self, request, format=None, *args, **kwargs):
        qs = tsumego_user_detail_data.objects.filter(user_id=request.user.pk, unique_problem_identifier=request.data.get('unique_problem_identifier'))
        df_detail = read_frame(qs)
        if len(df_detail) > 0:
            df_detail = df_detail.sort_values('answer_date', ascending=True)
            effort_list = df_detail['effort'].tolist()
            next_interval_minutes = int(sm2(x=effort_list)*24*60)
        else:
            next_interval_minutes = 720


        snippet = self.get_object(request.data.get('unique_problem_identifier'), request.user.pk)
        problem_information = request.data.dict()
        problem_information['last_seen'] = datetime.datetime.now(timezone.utc) + timedelta(hours=2)

        if snippet == None:
            problem_information['first_seen'] = datetime.datetime.now(timezone.utc) + timedelta(hours=2)
            problem_information['user_id'] = request.user.pk
        else:
            problem_information['user_id'] = request.user.pk
            time_difference = datetime.datetime.now(timezone.utc) + timedelta(hours=2) - snippet.last_seen
            problem_information['last_interval'] = int(time_difference.total_seconds())
        problem_information['scheduled_spaced'] = datetime.datetime.now(timezone.utc) + timedelta(minutes=next_interval_minutes) + timedelta(hours=2)
        serializer = TsumegoDataSerializer(snippet, data=problem_information)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetermineNextProblem(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #if qs == 0 bij bekijken van scheduled_spaced dan een random tsumego toewijzen
        qs = tsumego_user_data.objects.filter(user_id=request.user.pk)
        get_data = request.query_params

        df_total_all = read_frame(qs)
        all_problems_present = df_total_all["unique_problem_identifier"].tolist()
        qs = qs.filter(scheduled_spaced__lte=datetime.datetime.now(timezone.utc) + timedelta(hours=2))
        df_total = read_frame(qs)
        #if get_data['type'] is None:
        if len(df_total) > 0:
            df_total = df_total.sort_values("scheduled_spaced", ascending=True)
            next_problem_number = df_total["unique_problem_identifier"].tolist()[0]

            return Response({"next_problem": next_problem_number})
        else:
            print('NIETS OP DE PLANNING')
            qs = tsumego_overview.objects.all()
            player_rating = int(get_data['rating'])
            if player_rating is None:
                player_rating = 1000
            if player_rating < 100:
                player_rating = 1000
            df_total = read_frame(qs)
            df_total_zero = df_total[df_total['tsumego_rating'] == 0]
            df_total_zero = df_total_zero[~df_total_zero['unique_problem_identifier'].isin(all_problems_present)]
            if len(df_total_zero) > 0:
            	next_problem_number = df_total_zero["unique_problem_identifier"].tolist()[0]
            	return Response({"next_problem": next_problem_number})
            else:
                irand = randrange(0, 100)
                #streak change irand borders?
                if irand <= 20:
                    df_total['diff_rating'] = abs((df_total['tsumego_rating']) -  (player_rating+200))
                elif (irand > 20 and irand <= 30):
                    df_total['diff_rating'] = abs((df_total['tsumego_rating']) -  0)
                elif irand >= 80:
                    df_total['diff_rating'] = abs((df_total['tsumego_rating']) -  (player_rating-200))
                else:
                    df_total['diff_rating'] = abs(df_total['tsumego_rating'] -  player_rating)
                df_total = df_total.sort_values("diff_rating", ascending=True)
                df_total = df_total[~df_total['unique_problem_identifier'].isin(all_problems_present)]
                if len(df_total) > 0:
                    next_problem_number = df_total["unique_problem_identifier"].tolist()[0]
                    return Response({"next_problem": next_problem_number})
                else:
                    #hier nog oplossing?
                    next_problem_number = random.choice(all_problems_present)
                    return Response({"next_problem": next_problem_number})


class TsumegoExtendedData(APIView):
    def post(self, request, format=None, *args, **kwargs):

        problem_number = int(request.data.get('unique_problem_identifier'))
        count_problem = int(request.data.get('countProblem'))
        problem_correct = int(request.data.get('problemCorrect'))
        hint_used = int(request.data.get('hintUsed'))
        time_problem = int(request.data.get('timeProblem'))
        self_estimation = int(request.data.get('selfEstimation'))

        if self_estimation == 3:
            if count_problem == 1 and problem_correct == 1 and time_problem < 31:
                effort = 5
            elif problem_correct == 1 and time_problem < 61:
                effort = 4
            else:
                effort = 3
        elif self_estimation == 2:
            if count_problem == 1  and problem_correct == 1 and time_problem < 46 and hint_used == 0:
                effort = 4
            else:
                effort = 3
        elif self_estimation == 1:
            if count_problem < 3  and problem_correct == 1 and time_problem < 91 and hint_used == 0:
                effort = 3
            elif count_problem < 3 and hint_used < 3 and problem_correct == 1:
                effort = 2
            else:
                effort = 1
        else:
            effort = 0


        if hint_used > 0:
            problem_correct = 0
        else:
            problem_correct = problem_correct
        if count_problem > 1:
            problem_correct = 0
        else:
            problem_correct = problem_correct


        answer_date = datetime.datetime.now(timezone.utc) + timedelta(hours=2)

        detailed_data = {'user_id': request.user.pk, 'unique_problem_identifier': request.data.get('unique_problem_identifier'),
        'time_spent' : time_problem, 'effort': effort, 'correct': problem_correct, 'answer_date': answer_date, 'attempts': count_problem,
        'hints_used': hint_used
        }

        serializer = TsumegoExtendedDataSerializer(data=detailed_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TsumegoUserStats(APIView):
    def get(self, request, format=None, *args, **kwargs):
        #problem_number = int(request.data.get('unique_problem_identifier'))
        get_data = request.query_params
        qs = tsumego_user_detail_data.objects.filter(user_id=request.user.pk, unique_problem_identifier=get_data['id'])
        user_stats = read_frame(qs)
        if len(user_stats) < 1:
            status = 'Learning'
            return Response({"status": status, "last_seen": "N/A", "correct_perc": "N/A", "average_time":"N/A", "fastest_time": "N/A", "times_seen": "0"})
        else:
            df_correct = user_stats[user_stats['correct'] == 1]
            status = 'Remembering'
            times_seen = len(user_stats['correct'])
            fastest_time = df_correct['time_spent'].min()
            average_time = df_correct['time_spent'].mean()
            correct_perc = (len(df_correct['correct']) / len(user_stats['correct'])) * 100
            last_seen = user_stats['answer_date'].max()
            if correct_perc == 0:
                return Response({"status": status, "last_seen": last_seen, "correct_perc": correct_perc, "average_time":"N/A", "fastest_time": "N/A", "times_seen": times_seen})
            else:
                return Response({ "last_seen": last_seen, "correct_perc": correct_perc, "average_time": average_time, "fastest_time": fastest_time, "status": status, "times_seen": times_seen
                })


class TsumegoCommunityStats(APIView):
    def get(self, request, format=None, *args, **kwargs):
        get_data = request.query_params
        qs = tsumego_user_detail_data.objects.filter(unique_problem_identifier=get_data['id'])
        community_stats = read_frame(qs)
        if len(community_stats) < 1:
            return Response({"correct_perc": 'N/A', "fastest_time": "N/A", "average_time": "N/A"})
        else:
            df_correct = community_stats[community_stats['correct'] == 1]
            fastest_time = df_correct['time_spent'].min()
            average_time = df_correct['time_spent'].mean()
            correct_perc = (len(df_correct['correct']) / len(community_stats['correct'])) * 100
            if correct_perc == 0:
                return Response({"correct_perc": correct_perc, "average_time":"N/A", "fastest_time": "N/A"})
            else:
                return Response({"correct_perc": correct_perc, "average_time":average_time, "fastest_time": fastest_time})

class TsumegoOverallStats(APIView):
    def get(self, request, format=None, *args, **kwargs):
        get_data = request.query_params
        qs = tsumego_user_detail_data.objects.filter(user_id=request.user.pk)
        qs_user = user_progress.objects.filter(user_id=request.user.pk)

        print('WAT, HOEZO')
        overall_stats = read_frame(qs)
        if len(overall_stats) > 1:
            amount_tsumego = len(overall_stats)
            user_stats = read_frame(qs_user)
            highest_rating = user_stats['current_rating'].max()
            lowest_rating = user_stats['current_rating'].min()
            user_stats['date'] = user_stats['entry_date'].dt.date
            user_stats = user_stats.drop_duplicates(subset=['date'], keep='first')
            rank_per_day = user_stats.groupby(['date'])['current_rating'].mean().reset_index()

            unique_problems = list(set(overall_stats['unique_problem_identifier'].tolist()))

            qs_tsumegos = tsumego_overview.objects.filter(unique_problem_identifier__in=unique_problems)
            overall_stats_tsumego = read_frame(qs_tsumegos)
            overall_stats = overall_stats.merge(overall_stats_tsumego, left_on='unique_problem_identifier', right_on='unique_problem_identifier', how='left')
            #print(overall_stats_tsumego)
            #print(overall_stats)
            overall_stats['day'] = overall_stats['answer_date'].dt.date
            overall_stats['rank'] = overall_stats['tsumego_rating'].map(rating_mapper)
            total_time = int(overall_stats['time_spent'].sum() / 60)
            total_perc = overall_stats.groupby(['correct']).size()
            amount_per_day = overall_stats.groupby(['day']).size().reset_index().rename(columns={0: 'count'})
            perc_per_day = overall_stats.groupby(['day', 'correct']).size().groupby(level=[0]).apply(lambda g: (g / g.sum() * 100)).reset_index().rename(columns={0: 'perc'})
            perc_per_day = perc_per_day[perc_per_day['correct'] == 1]
            total_perc = int((total_perc[1] / (total_perc[0] + total_perc[1])) * 100)
            perc_per_rank = overall_stats.groupby(['tsumego_rating','rank', 'correct']).size().groupby(level=[0]).apply(lambda g: (g / g.sum()) * 100).reset_index().rename(columns={0: 'perc'})
            perc_per_rank = perc_per_rank[perc_per_rank['correct'] == 1]
            time_per_rank = overall_stats.groupby(['tsumego_rating','correct'])['time_spent'].mean().reset_index()
            time_per_rank = time_per_rank[time_per_rank['correct'] == 1]

            #print(perc_per_rank)
            print(overall_stats.groupby(['tsumego_rating', 'rank', 'correct']).size())

            #community_stats = read_frame(qs)
            return Response({
                             'amount_of_games': amount_tsumego,
                             'lowest_rating': lowest_rating,
                             'highest_rating': highest_rating,
                             'rank_per_day': rank_per_day,
                             'total_perc': total_perc,
                             'total_time': total_time,
                             'amount_per_day': amount_per_day,
                             'perc_correct': perc_per_day,
                             'perc_correct_per_rank': perc_per_rank})
        else:
            return Response({})


#sessionstats


def sm2(x: [int], a=6.0, b=-0.8, c=0.28, d=0.02, theta=0.2) -> float:
    """
    Returns the number of days to delay the next review of an item by, fractionally, based on the history of answers x to
    a given question, where
    x == 0: Incorrect, Hardest
    x == 1: Incorrect, Hard
    x == 2: Incorrect, Medium
    x == 3: Correct, Medium
    x == 4: Correct, Easy
    x == 5: Correct, Easiest
    @param x The history of answers in the above scoring.
    @param theta When larger, the delays for correct answers will increase.
    """
    assert all(0 <= x_i <= 5 for x_i in x)
    correct_x = [x_i >= 3 for x_i in x]
    # If you got the last question incorrect, just return 1
    if not correct_x[-1]:
        return 1.0

    # Calculate the latest consecutive answer streak
    num_consecutively_correct = 0
    for correct in reversed(correct_x):
        if correct:
            num_consecutively_correct += 1
        else:
            break

    return a*(max(1.3, 2.5 + sum(b+c*x_i+d*x_i*x_i for x_i in x)))**(theta*num_consecutively_correct)
