from django.apps import AppConfig


class TsumegoRegisterConfig(AppConfig):
    name = 'tsumego_register'
