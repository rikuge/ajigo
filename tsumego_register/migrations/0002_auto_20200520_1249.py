# Generated by Django 2.1 on 2020-05-20 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tsumego_register', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tsumego_user_data',
            name='first_seen',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='tsumego_user_data',
            name='last_seen',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='tsumego_user_data',
            name='scheduled_spaced',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
