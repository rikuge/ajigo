from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.utils import timezone

#Each row is a record of how an user has interacted with a specific tsumego
class tsumego_user_data(models.Model):
    unique_problem_identifier = models.IntegerField(null=True, blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    first_seen = models.DateTimeField(null=True,blank=True)
    last_seen = models.DateTimeField(null=True,blank=True)
    scheduled_spaced = models.DateTimeField(null=True,blank=True)
    last_interval = models.IntegerField(null=True, blank=True)
    blacklist = models.IntegerField(null=True, blank=True)

class tsumego_user_stats(models.Model):
    unique_problem_identifier = models.IntegerField(null=True, blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    times_seen = models.IntegerField(null=True, blank=True)
    times_correct = models.IntegerField(null=True, blank=True)
    times_incorrect = models.IntegerField(null=True, blank=True)
    best_time = models.IntegerField(null=True, blank=True)
    #extended_stats = JSONField() #saves an object at the end of the jsonField for each attempt with extended stats, can be accessed with button see extended stats

class tsumego_user_detail_data(models.Model):
    unique_problem_identifier = models.IntegerField(null=True, blank=True)
    user_id = models.IntegerField(null=True, blank=True)
    time_spent = models.IntegerField(null=True, blank=True) #in seconds
    effort = models.IntegerField(null=True, blank=True)
    correct = models.IntegerField(null=True, blank=True)
    answer_date = models.DateTimeField(null=True,blank=True)
    attempts = models.IntegerField(null=True, blank=True)
    hints_used = models.IntegerField(null=True, blank=True)
      



#
