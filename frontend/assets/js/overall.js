var current_problem = 0
var current_problem_rating = 0
var current_problem_rating_phi = 0
var current_player_rating = 0
var times_seen = 0







$(function () {
    $("#next_problem_easy").click(function () {
      pushPlayData("/tsumego/register/tsumego_data/")
      NewRating('/user/update_rating/', current_problem_rating, current_problem_rating_phi)
      setTimeout(function(){ getnextProblem("/tsumego/next/" + "?rating=" +  current_player_rating.toString()) }, 200)
      ExtendedPlayData("/tsumego/register/tsumego_extended_data/", 3)
      resetStats()
      setTimeout($("#clear").click(), 500)



    });
});



$(function () {
    $("#next_problem_average").click(function () {
      pushPlayData("/tsumego/register/tsumego_data/")
      NewRating('/user/update_rating/', current_problem_rating, current_problem_rating_phi)
      setTimeout(function(){ getnextProblem("/tsumego/next/" + "?rating=" +  current_player_rating.toString()) }, 200)
      ExtendedPlayData("/tsumego/register/tsumego_extended_data/", 2)
      resetStats()
      setTimeout($("#clear").click(), 500)



    });
});

$(function () {
    $("#next_problem_hard").click(function () {
      pushPlayData("/tsumego/register/tsumego_data/")
      NewRating('/user/update_rating/', current_problem_rating, current_problem_rating_phi)
      setTimeout(function(){ getnextProblem("/tsumego/next/" + "?rating=" +  current_player_rating.toString()) }, 200)
      ExtendedPlayData("/tsumego/register/tsumego_extended_data/", 1)
      resetStats()
      setTimeout($("#clear").click(), 500)




    });
});

$(function () {
    $("#next_problem_forgotten").click(function () {
      pushPlayData("/tsumego/register/tsumego_data/")
      NewRating('/user/update_rating/', current_problem_rating, current_problem_rating_phi)
      setTimeout(function(){ getnextProblem("/tsumego/next/" + "?rating=" +  current_player_rating.toString()) }, 200)
      ExtendedPlayData("/tsumego/register/tsumego_extended_data/", 0)
      resetStats()

      setTimeout($("#clear").click(), 500)




    });
});

function resetStats(endpoint){
  countProblem = 0;
  timeProblem = 0;
  problemCorrect = 0;
  hintUsed = 0;

  document.getElementById("next_problem_easy").style.pointerEvents = "none";
  document.getElementById("next_problem_average").style.pointerEvents = "none";
  document.getElementById("easy_button").style.background='#808080';
  document.getElementById("easy_button").style.border='#808080';
  document.getElementById("average_button").style.background='#808080';
  document.getElementById("average_button").style.border='#808080';

};


function pushPlayData(endpoint){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      url: endpoint,
      method: "PUT",
      data: {"unique_problem_identifier": current_problem},
      headers:{
        "X-CSRFToken": csrftoken
      },
      dataType: "json",
      success: function(result){
        console.log(result)
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};


function ExtendedPlayData(endpoint, selfEstimation){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      url: endpoint,
      method: "POST",
      data: {"unique_problem_identifier": current_problem,
      "countProblem": countProblem,
      "problemCorrect": problemCorrect,
      "hintUsed": hintUsed,
      "timeProblem": timeProblem,
      "selfEstimation": selfEstimation
      },
      headers:{
        "X-CSRFToken": csrftoken
      },
      dataType: "json",
      success: function(result){
        console.log(result)
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};


function NewRating(endpoint, current_problem_rating, current_problem_rating_phi){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      url: endpoint,
      method: "POST",
      data: {"current_problem_rating": current_problem_rating,
      "current_problem_rating_phi": current_problem_rating_phi,
      "times_seen": times_seen,
      "problem_correct": problemCorrect,
      "hints_used": hintUsed,
      "count_problem": countProblem,
      },
      headers:{
        "X-CSRFToken": csrftoken
      },
      dataType: "json",
      success: function(result){
        console.log(result.current_rating)
        getRating('/user/rating/')
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};


function getRating(endpoint){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      method: "GET",
      url: endpoint,
      success: function(data){
        current_rating = data.current_rating
        $('#urating').text(current_rating)

        current_player_rating = data.rating_number
        console.log('Player_rating', current_player_rating)
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};



function getEndpoint(endpoint){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      method: "GET",
      url: endpoint,
      success: function(data){
        current_problem_rating = data.problem_rating
        $('#uproblemrating').text(current_problem_rating.toString())


        current_problem_rating_phi = data.problem_rating_phi
        var tsumego = new WGo.Tsumego(document.getElementById("tsumego_wrapper"), {
        	sgf: data.problem,
        	//debug: true, /* remove this line hide solution */
        });
        tsumego.setCoordinates(true);

        //setChart(labels, defaultData, temp_delict, subtitle, container, description, bronnen)

      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};

function getnextProblem(endpoint){
  var endpoint = endpoint
  //var temp_area = $("#AdvancedSearch1 .selection").text()

  $.ajax({
      method: "GET",
      url: endpoint,
      success: function(data){
        var endpoint_url =  '/api/problems?id=' + data.next_problem.toString()
        current_problem = data.next_problem

        getEndpoint(endpoint_url)

        getUserTsumegoStats(data.next_problem)
        getCommunityTsumegoStats(data.next_problem)

        //setChart(labels, defaultData, temp_delict, subtitle, container, description, bronnen)

      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};

function getUserTsumegoStats(current_problem){
  console.log(current_problem)
  var endpoint =  '/tsumego/tsumego_stats/?id=' + current_problem.toString()
  //var temp_area = $("#AdvancedSearch1 .selection").text()
  $.ajax({
      method: "GET",
      url: endpoint,
      success: function(data){
        console.log('WAT', data)
        times_seen = data.times_seen
        $('#utimes').text(data.times_seen)
        $('#ulast').text(data.last_seen)
        $('#uaverage').text(data.average_time)
        $('#ufast').text(data.fastest_time)
        $('#ucorrect').text(data.correct_perc)
        $('#ustatus').text(data.status)
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};


function getCommunityTsumegoStats(current_problem){
  console.log(current_problem)
  var endpoint =  '/tsumego/community_stats/?id=' + current_problem.toString()
  //var temp_area = $("#AdvancedSearch1 .selection").text()
  $.ajax({
      method: "GET",
      url: endpoint,
      success: function(data){
        $('#ucommunityaverage').text(data.average_time)
        $('#ucommunityfast').text(data.fastest_time)
        $('#ucommunitycorrect').text(data.correct_perc)
      },
      error: function(error_data){
          console.log("error")
          console.log(error_data)
      }
  })
};





// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
console.log('CSRF:', csrftoken)



$(document).ready(function() {
StartTimer()
})

function StartTimer(){
var h1 = document.getElementsByTagName('h4')[0],
    seconds = 0, minutes = 0, hours = 0,
    t;




function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    h1.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    timer();
}
function timer() {
    t = setTimeout(add, 1000);
}
timer();


stop.onclick = function() {
    clearTimeout(t);
}

clear.onclick = function() {
    h1.textContent = "00:00:00";
    seconds = 0; minutes = 0; hours = 0;
}





}
