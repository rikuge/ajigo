from rest_framework import serializers
from rest_framework.serializers import (
    HyperlinkedIdentityField,
    HyperlinkedRelatedField,
    ModelSerializer,
)

from .models import *
from django.conf import settings


class UserProgressSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_progress
        fields = ("user_id", "current_rating", "current_rating_phi", "entry_date")
