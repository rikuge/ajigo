from django.shortcuts import render
from django.views.generic import View
from django.db.models.functions import TruncHour, TruncDay, TruncMonth
import datetime
from datetime import timedelta

from django.db.models import Count, Sum, F
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework import viewsets, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import SessionAuthentication
from rest_framework import generics
from django_pandas.io import read_frame
from datetime import date, timedelta
from django.db.models import Q
import random
import math

import calendar
import pandas as pd
import datetime as dt

from tsumego.models import tsumego_overview

from .models import *
from .serializers import *

from django.views.decorators.csrf import csrf_exempt
from .glicko2 import Glicko2, WIN, DRAW, LOSS

rating_mapper = {
                 300: '18K',  350: '18K+', 400: '17K',  450: '17K+', 500: '16K', 550: '16K+',
                 600: '15K',  650: '15K+', 700: '14K',  750: '14K+', 800: '13K', 850: '13K+',
                 900: '12K',  950: '12K+', 1000: '11K', 1050: '11K+', 1100: '10K', 1150: '10K+',
                 1200: '9K',  1250: '9K+', 1300: '8K',  1350: '8K+', 1400: '7K', 1450: '7K+',
                 1500: '6K',  1550: '6K+', 1600: '5K',  1650: '5K+', 1700: '4K', 1750: '4K+',
                 1800: '3K',  1850: '3K+', 1900: '2K',  1950: '2K+', 2000: '1K', 2050: '1K+',
                 2100: '1D',  2150: '1D+', 2200: '2D',  2250: '2D+', 2300: '3D', 2350: '3D+',
                 2400: '4D',  2450: '4D+', 2500: '5D',  2550: '5D+', 2600: '6D', 2650: '6D+',
                 2700: '7D',  2750: '7D+', 2800: '8D',  2850: '8D+', 2900: '9D', 2950: '9D+',
                }

def calculate_rating(user_rating, problem_rating, condition, times_seen):
    env = Glicko2(tau=0.5)
    r1 = env.create_rating(user_rating[0], user_rating[1], 0.06)
    r2 = env.create_rating(problem_rating[0], problem_rating[1])
    if condition == 1:
        rated = env.rate(r1, [(WIN, r2)])
    elif condition == 0:
        rated = env.rate(r1, [(DRAW, r2)])
    else:
        rated = env.rate(r1, [(LOSS, r2)])

    if rated.mu > user_rating[0]:
        if times_seen <= 1:
            new_rating = rated.mu
        elif times_seen == 2:
            new_rating = ((rated.mu - user_rating[0]) * 0.75) + user_rating[0]
        elif times_seen == 3:
            new_rating = ((rated.mu - user_rating[0]) * 0.5) + user_rating[0]
        else:
            new_rating = ((rated.mu - user_rating[0]) * 0.25) + user_rating[0]
    elif rated.mu < user_rating[0]:
        if times_seen <= 1:
            new_rating = rated.mu
        elif times_seen == 2:
            new_rating = ((rated.mu - user_rating[0]) * 1.25) + user_rating[0]
        else:
            new_rating = ((rated.mu - user_rating[0]) * 1.5) + user_rating[0]
    else:
        new_rating = rated.mu
    rated_scores = [new_rating, rated.phi]
    return rated_scores


class UserRating(APIView):
    def get(self, request, format=None, *args, **kwargs):
        qs = user_progress.objects.filter(user_id=request.user.pk)
        df_total_all = read_frame(qs)
        df_total_all.sort_values('entry_date', ascending=False, inplace=True)


        if len(df_total_all) < 1:
            return Response({"current_rating": 1000, 'rating_number': 1000})
        else:
            df_total_all['current_rating_fixed'] = math.ceil(df_total_all['current_rating'].tolist()[0] / 50.0) * 50.0
            df_total_all['rank'] = df_total_all['current_rating_fixed'].map(rating_mapper)
            rating_text = 'Rating: ' + str(df_total_all['current_rating'].tolist()[0]) + '(' + str(df_total_all['rank'].tolist()[0]) + ')'
            return Response({"current_rating": rating_text, 'rating_number': df_total_all['current_rating'].tolist()[0]})



class DetermineNextRating(APIView):
    def post(self, request, format=None, *args, **kwargs):
        qs = user_progress.objects.filter(user_id=request.user.pk)
        problem_rating = int(request.data.get('current_problem_rating'))
        problem_rating_phi = int(request.data.get('current_problem_rating_phi'))
        times_seen = int(request.data.get('times_seen'))
        problem_correct = int(request.data.get('problem_correct'))
        hints_used = int(request.data.get('hints_used'))
        count_problem = int(request.data.get('count_problem'))
        if problem_correct == 1:
            if count_problem == 1:
                if hints_used == 0:
                    condition = 1
                else:
                    condition = 2
            elif count_problem == 2:
                if hints_used < 2:
                    condition = 2
                else:
                    condition = 2

            else:
                condition = 2
        else:
            condition = 2

        df_total_all = read_frame(qs)
        if len(df_total_all) < 1:
            new_scores = calculate_rating([1000, 350], [problem_rating, problem_rating_phi], condition, times_seen)
            new_rating = new_scores[0]
            new_phi = new_scores[1]
            if problem_rating == 0:
                detailed_data = {"user_id": request.user.pk, "current_rating": 1000, "current_rating_phi": 350, "entry_date": datetime.datetime.now(timezone.utc) + timedelta(hours=2)}
            else:
                detailed_data = {"user_id": request.user.pk, "current_rating": int(new_rating), "current_rating_phi": int(new_phi), "entry_date": datetime.datetime.now(timezone.utc) + timedelta(hours=2)}
            serializer = UserProgressSerializer(data=detailed_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                print(serializer.errors)
                print('False')
        else:
            df_total_all.sort_values('entry_date', ascending=False, inplace=True)
            current_rating = df_total_all['current_rating'].tolist()[0]
            current_rating_phi = df_total_all['current_rating_phi'].tolist()[0]
            new_scores = calculate_rating([current_rating, current_rating_phi], [problem_rating, problem_rating_phi], condition, times_seen)
            new_rating = new_scores[0]
            new_phi = new_scores[1]
            if problem_rating == 0:
                detailed_data = {"user_id": request.user.pk, "current_rating": int(current_rating), "current_rating_phi": int(current_rating_phi), "entry_date": datetime.datetime.now(timezone.utc) + timedelta(hours=2)}
            else:
                detailed_data = {"user_id": request.user.pk, "current_rating": int(new_rating), "current_rating_phi": int(new_phi), "entry_date": datetime.datetime.now(timezone.utc) + timedelta(hours=2)}
            serializer = UserProgressSerializer(data=detailed_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
