from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.utils import timezone

#Each row is a record of how an user has interacted with a specific tsumego
class user_progress(models.Model):
    user_id = models.IntegerField(null=True, blank=True)
    current_rating = models.IntegerField(null=True, blank=True)
    current_rating_phi = models.IntegerField(null=True, blank=True)
    entry_date  = models.DateTimeField(null=True,blank=True)
