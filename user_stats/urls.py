from django.urls import path, include
from django.contrib import admin

from .views import *
from django.views.generic import TemplateView

urlpatterns = [
    path('rating/' , UserRating.as_view(), name='UserRating'),
    path('update_rating/' , DetermineNextRating.as_view(), name='UpdateRating'),

]
